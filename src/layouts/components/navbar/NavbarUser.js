import React from "react"
import {
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Button
} from "reactstrap"
import * as Icon from "react-feather"
import { history } from "../../../history"


const UserDropdown = props => {
  return (
    <DropdownMenu right>
      <DropdownItem tag="a" href="#">
        <Icon.User size={14} className="mr-50" />
        <span className="align-middle">Edit Profile</span>
      </DropdownItem>
      <DropdownItem tag="a" href="#">
        <Icon.Mail size={14} className="mr-50" />
        <span className="align-middle">My Inbox</span>
      </DropdownItem>
      <DropdownItem tag="a" href="#">
        <Icon.CheckSquare size={14} className="mr-50" />
        <span className="align-middle">Tasks</span>
      </DropdownItem>
      <DropdownItem tag="a" href="#">
        <Icon.MessageSquare size={14} className="mr-50" />
        <span className="align-middle">Chats</span>
      </DropdownItem>
      <DropdownItem tag="a" href="#">
        <Icon.Heart size={14} className="mr-50" />
        <span className="align-middle">WishList</span>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem
        tag="a"
        href="#"
        onClick={e => history.push("/pages/login")}
      >
        <Icon.Power size={14} className="mr-50" />
        <span className="align-middle">Log Out</span>
      </DropdownItem>
    </DropdownMenu>
  )
}

const NavbarUser = (props) => {
  const {userName, userImg} = props
  return (
    <ul className="nav navbar-nav navbar-nav-user float-right align-items-center">
        <NavItem className="nav-search">
          <NavLink className="nav-link-search">
            <Button.Ripple color="primary" type="submit" onClick={() => history.push("/")}>
              Export
            </Button.Ripple>
          </NavLink>
          <NavLink className="nav-link-search">
            <Button.Ripple color="success" type="submit" onClick={() => history.push("/")}>
              Generate
            </Button.Ripple>
          </NavLink>
          <NavLink className="nav-link-search">
            <Button.Ripple color="warning" type="submit" onClick={() => history.push("/")}>
              Save
            </Button.Ripple>
          </NavLink>
        </NavItem>
        <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
          <DropdownToggle tag="a" className="nav-link dropdown-user-link">
            <div className="user-nav d-sm-flex d-none">
              <span className="user-name text-bold-600">
                {userName}
              </span>
              <span className="user-status">Available</span>
            </div>
            <span data-tour="user">
              <img
                src={userImg}
                className="round"
                height="40"
                width="40"
                alt="avatar"
              />
            </span>
          </DropdownToggle>
          <UserDropdown {...props} />
        </UncontrolledDropdown>
      </ul>
  )
}

export default NavbarUser
