import React, { useState } from "react"
import {
  FormGroup, Label, Input, Row, Col, Card, CardTitle, CardText, Button
} from 'reactstrap';

const Home = () => {
  const [audience, setAudience] = useState('{Audience}')
  const [solution, setSolution] = useState('{Solution}')
  const [problem, setProblem] = useState('{Problem}')
  const [product, setProduct] = useState('{Product}')
  const [desireOutcome, setDesireOutcome] = useState('{Desire Outcome}')
  const [period, setPeriod] = useState('{period}')
  const [productName, setProductName] = useState('{Product Name}')
  const sideBars = [
    {
      id: 1,
      label: 'Audience',
      name: 'audience',
      placeholder: '{Audience}',
    },
    {
      id: 2,
      label: 'Solution',
      name: 'solution',
      placeholder: '{Solution}',
    },
    {
      id: 3,
      label: 'Problem',
      name: 'problem',
      placeholder: '{Problem}',
      logic: (e) => setAudience(e)
    },
    {
      id: 4,
      label: 'Product',
      name: 'product',
      placeholder: '{Product}',
    },
    {
      id: 5,
      label: 'Desire Outcome',
      name: 'desire',
      placeholder: '{Desire Outcome}',
    },
    {
      id: 6,
      label: 'Periode',
      name: 'periode',
      placeholder: '{Periode}',
    },
    {
      id: 7,
      label: 'Product Name',
      name: 'product',
      placeholder: '{Product Name}',
    },
  ]


  const contents = [
    {
      id: 1,
      status: 'Free',
      title: <p>Your Search for <b>{desireOutcome}</b> Ends Here</p>,
      value: audience,
      onChange: { setAudience }
    },
    {
      id: 2,
      status: 'Free',
      title: <p>Tired of <b>{problem}</b>? Get started with <b>{solution}</b> Today!</p>,
      value: solution,
      onChange: setSolution
    },
    {
      id: 3,
      status: 'Free',
      title: <p>Want <b>{desireOutcome}</b>? Get <b>{solution}</b> Now</p>,
      value: problem,
      onChange: setProblem
    },
    {
      id: 4,
      status: 'Free',
      title: <p><b>{audience}</b>: It's Time to Stop <b>{problem}</b>. Use <b>{solution}</b></p>,
      value: product,
      onChange: setProduct
    },
    {
      id: 5,
      status: 'Free',
      title: <p><b>{audience}</b> Show You How to <b>{desireOutcome}</b></p>,
      value: desireOutcome,
      onChange: setDesireOutcome
    },
    {
      id: 6,
      status: 'Free',
      title: <p>83 of <b>{audience}</b> Is Right/Wrong About <b>{desireOutcome}</b></p>,
    },
    {
      id: 7,
      status: 'Free',
      title: <p>The Biggest (And Easiest) Secret of <b>{desireOutcome}</b></p>,
      value: period,
      onChange: setPeriod
    },
    {
      id: 8,
      status: 'Free',
      title: <p>The Best Way for <b>{audience}</b> to Get <b>{desireOutcome}</b></p>,
      value: productName,
      onChange: setProductName
    },
    {
      id: 9,
      status: 'Free',
      title: <p>Start Saving Money on <b>{desireOutcome}</b></p>,
      value: audience,
      onChange: setAudience
    },
    {
      id: 10,
      status: 'Free',
      title: <p>The Biggest (And Easiest) Secret of <b>{desireOutcome}</b></p>,
      value: period,
      onChange: setPeriod
    },
    {
      id: 11,
      status: 'Free',
      title: <p>The Best Way for <b>{audience}</b> to Get {desireOutcome}</p>,
      value: productName,
      onChange: setProductName
    },
    {
      id: 12,
      status: 'Free',
      title: <p>Start Saving Money on <b>{desireOutcome}</b></p>,
      value: audience,
      onChange: setAudience
    },
  ]
  return (
    <div class="border-r bg-white flex flex-row h-screen overflow-hidden shadow">
      <Row>
        <div class="sidebar bg-white col-3 ml-1  flex-col justify-start h-screen border-r border-gray-800 position-fixed" style={{ zIndex: 999, overflowY: 'auto', height: '100%' }}>
          <div class="p-2" style={{ marginBottom: '40%' }}>
            <FormGroup>
              <Label for="exampleSelect">CATEGORY</Label>
              <Input type="select" name="select" id="exampleSelect" style={{ backgroundColor: '#F6F6F6' }}>
                <option>All categories</option>
                <option>Ad</option>
                <option>Email</option>
                <option>Article</option>
                <option>Landing Page</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="exampleSelect">LANGUAGE</Label>
              <Input type="select" name="select" id="exampleSelect" style={{ backgroundColor: '#F6F6F6' }}>
                <option>English</option>
                <option>Portuguese</option>
                <option>Spanish</option>
                <option>French</option>
                <option>German</option>
                <option>Dutch</option>
              </Input>
            </FormGroup>
            {/* <div class="mb-4 flex flex-col">
              {sideBars.map((item, index) => {
                return (
                  <FormGroup key={index}>
                    <Label for="exampleEmail">{item.label}</Label>
                    <Input type="text" name={item.name} id="exampleEmail" placeholder={item.placeholder} value={item.value}
                      onChange={(e) => setProblem(e.target.value)}/>
                  </FormGroup>
                )
              })}
            </div> */}
            <div class="mb-4 flex flex-col">
              <FormGroup>
                <Label for="exampleEmail">Audience</Label>
                <Input type="text" name="audience" id="exampleEmail" placeholder="{Audience}" value={audience}
                  onChange={(e) => setAudience(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Solution</Label>
                <Input type="text" name="solution" id="exampleEmail" placeholder="{Solutin}" value={solution}
                  onChange={(e) => setSolution(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Problem</Label>
                <Input type="text" name="problem" id="exampleEmail" placeholder="{Problem}" value={problem}
                  onChange={(e) => setProblem(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Product</Label>
                <Input type="text" name="product" id="exampleEmail" placeholder="{Product}" value={product}
                  onChange={(e) => setProduct(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Desire Outcome</Label>
                <Input type="text" name="desireoutcome" id="exampleEmail" placeholder="{Desire Outcome}" value={desireOutcome}
                  onChange={(e) => setDesireOutcome(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Period</Label>
                <Input type="text" name="periode" id="exampleEmail" placeholder="{Period}" value={problem}
                  onChange={(e) => setPeriod(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
              <FormGroup>
                <Label for="exampleEmail">Product Name</Label>
                <Input type="text" name="productname" id="exampleEmail" placeholder="{Product Name}" value={productName}
                  onChange={(e) => setProductName(e.target.value)} style={{ backgroundColor: '#F6F6F6' }} />
              </FormGroup>
            </div>
          </div>
        </div>
        <div class="col-8 md:flex ml-auto flex-col h-screen border-r border-gray-800 dark:border-grey-200 ">
          <div class="p-2">
            <h2 class="title-content" style={{fontSize: 22, color: 'rgb(115, 103, 240)', fontWeight: 'bold' }}>Copywriter Template</h2>
            <div class="mb-4 mt-2">
              <Row>
                {contents.map((contents, index) => {
                  return (
                    <Col sm="4" key={index} style={{marginBottom: 10}}>
                      <Row>
                        <div class="ml-auto mr-1">
                          <button class="btn btn-primary btn-sm" style={{ marginRight: 5 }}>Copy</button>
                          <button class="btn btn-primary btn-sm">Select</button>
                          {/* <Button size="sm">Copy</Button> */}
                        </div>
                      </Row>
                      <Card body style={{height: '80%', width: '100%', justifyContent: 'center', marginTop: 5}}>
                        {/* <CardTitle>{contents.status}</CardTitle> */}
                        <CardText style={{ textAlign: 'center' }}>{contents.title}</CardText>
                      </Card>
                    </Col>
                  )
                })}
              </Row>
            </div>
          </div>
        </div>
      </Row>
    </div>
  )
}

export default Home