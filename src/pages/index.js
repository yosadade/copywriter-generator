import Login from "./Login"
import Register from "./Register"
import Home from "./Home"
import Email from "./Email"
import Chart from "./Chart"

export {Login, Register, Home, Email, Chart}